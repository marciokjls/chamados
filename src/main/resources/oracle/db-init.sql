connect system/oracle as sysdba
alter session set "_ORACLE_SCRIPT"=true;
--**********************************
--Esquema chamados
--**********************************

create tablespace chamados datafile '/opt/oracle/oradata/chamados01.dbf' size 400M online;
create tablespace idx_chamados datafile '/opt/oracle/oradata/idx_chamados01.dbf' size 400M;
create user chamados identified by chamados default tablespace chamados temporary tablespace temp;
alter user chamados quota unlimited on chamados;
grant resource to chamados;
grant connect to chamados;
grant create view to chamados;
grant create job to chamados;
grant create procedure to chamados;
grant create materialized view to chamados;
alter user chamados default role connect, resource;

exit;
