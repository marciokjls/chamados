package com.api.chamados.domain;


import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "CHAMADO")
@Data
public class Chamado {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CHAMADO")
    @SequenceGenerator(name = "SEQ_CHAMADO", sequenceName = "SEQ_CHAMADO")
    private Long Id;

    @Column(name = "CODIGO")
    private String codigo;

    @Column(name = "TITULO")
    private String titulo;

    @Column(name = "DESCRICAO")
    private String descricao;

    //@Column(name = "ANEXOS")

    @ManyToOne
    @JoinColumn(name = "EXECUTOR_ID")
    private Usuario usuario;


    @Column(name = "ATIVIDADE_CONCLUIDA", columnDefinition = "boolean default false")
    private boolean atividadeConcluida;
}
