package com.api.chamados.domain;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "USUARIO")
@Data
public class Usuario {

    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_USUARIO")
    @SequenceGenerator(name = "SEQ_USUARIO", sequenceName = "SEQ_USUARIO")
    private Long Id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "UNIDADE_ID")
    private Unidade unidade;
}
