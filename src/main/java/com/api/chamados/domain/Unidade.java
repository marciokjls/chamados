package com.api.chamados.domain;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "UNIDADE")
@Data
public class Unidade {

    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_UNIDADE")
    @SequenceGenerator(name = "SEQ_UNIDADE", sequenceName = "SEQ_UNIDADE")
    private Long Id;

    @Column(name = "SIGLA")
    private String sigla;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "UNIDADE_SUPERIOR_ID")
    @OneToOne
    private Unidade unidade;

}
